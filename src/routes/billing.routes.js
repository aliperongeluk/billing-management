import express from 'express';
const routes = express.Router();

import { sendSomethingWentWrongResponse } from '../utils/response';

import Billing from '../models/billing.model';

/**
 * @swagger
 * /api/billing-management/billing:
 *    get:
 *     tags:
 *       - Billing
 *     description: Retrieve all billings.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Billings are returned.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Billing'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', async (req, res) => {
  try {
    const billings = await Billing.find();
    res.send(billings);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

export default routes;
