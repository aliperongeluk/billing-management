const constants = {
  EXCHANGE_BILLING: 'billing',
  EXCHANGE_PAYMENT: 'payment',
  SERVICE_NAME: 'billing-management',
};

module.exports = constants;
