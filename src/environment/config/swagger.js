import swaggerJsDoc from 'swagger-jsdoc';

const options = {
  swaggerDefinition: {
    info: {
      title: 'Billing Management',
      version: '1.0',
      description:
        'Swagger documentation for Billing Management microservice (AliPerOngeluk).',
    },
  },

  apis: ['./src/routes/*.js', './src/models/*.js'],
};

const specs = swaggerJsDoc(options);

export default specs;
