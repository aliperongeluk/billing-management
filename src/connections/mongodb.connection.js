import mongoose from 'mongoose';
import { dbUrl } from '../environment/config/mongodb.config';

mongoose.Promise = global.Promise;

mongoose.connect(dbUrl, { useNewUrlParser: true });
const connection = mongoose.connection
  .once('open', () => {
    console.log('Connected to Mongo on ' + dbUrl);
  })
  .on('error', error => {
    console.warn('Warning', error.toString());
  });

export default connection;
