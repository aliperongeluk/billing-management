const constants = require('../environment/constants/exchange');
const paymentConsumers = require('./consumers/payment.consumers');
let messageChannel;

const startConsuming = () => {
  createExchanges();
  paymentConsumers.consume(messageChannel);
};

const createExchanges = () => {
  messageChannel.assertExchange(constants.EXCHANGE_PAYMENT, 'topic', {
    durable: true,
  });
};

const setMessageChannel = channel => {
  messageChannel = channel;
  startConsuming();
};

module.exports = { setMessageChannel };
