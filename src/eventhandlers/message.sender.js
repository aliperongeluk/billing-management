let messageChannel;
const constants = require('../environment/constants/exchange');

const publish = (topic, payload) => {
  messageChannel.publish(
    constants.EXCHANGE_BILLING,
    topic,
    Buffer.from(JSON.stringify(payload)),
    {
      persistent: true,
    }
  );
};

const setMessageChannel = channel => {
  messageChannel = channel;
};

module.exports = { setMessageChannel, publish };
