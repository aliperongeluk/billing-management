const constants = require('../../environment/constants/exchange');
const events = require('../../environment/constants/events');
const messageSender = require('../message.sender');
const Billing = require('../../models/billing.model');

const consumerPaymentCompleted = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.PAYMENT_COMPLETED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_PAYMENT,
        events.PAYMENT_COMPLETED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          try {
            const payment = JSON.parse(message.content.toString());
            const billing = new Billing({
              order: payment.orderId,
              payment: payment._id,
              customerInformationId: payment.customerInformationId,
            });
            await billing.save();
            messageSender.publish('INVOICE_CREATED', billing);
          } catch (error) {
            // eslint-disable-next-line no-console
            console.log(error);
          }
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumerPaymentCompleted(messageChannel);
};

module.exports = { consume };
