import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const BillingSchema = new Schema({
  order: {
    type: String,
    required: [true, 'order is required.'],
  },
  payment: {
    type: String,
    required: [true, 'payment is required.'],
  },
  customerInformationId: {
    type: String,
    required: [true, 'The order needs to be paid by someone, right???'],
  },
});

const Billing = mongoose.model('billing', BillingSchema);

module.exports = Billing;

/**
 * @swagger
 * definitions:
 *  Billing:
 *    type: object
 *    properties:
 *      _id:
 *        type: string
 *      order:
 *        type: string
 *      payment:
 *        type: string
 *      customerInformationId:
 *        type: string
 *      createdAt:
 *        type: string
 *        format: date-time
 *      updatedAt:
 *        type: string
 *        format: date-time
 *      __v:
 *        type: integer
 */
