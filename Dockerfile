FROM node:10.13.0

WORKDIR /home/docktor/compositions/billing-management

COPY package*.json ./

RUN npm install
RUN npm install pm2 -g

COPY . .

RUN npm run build

EXPOSE 7009

CMD ["pm2-runtime", "dist/server.js"]
